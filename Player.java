import org.lwjgl.input.Mouse;

public class Player {
	
	public Player() {
		rotation = 0F;
	}
	
	public float getRotation() {
		return rotation;
	}
	
	public void update(CubeRunner game) {
		posX += rotation * (game.slowMotion ? 0.0004F : 0.0008F);
		float f = (Mouse.getX() - CubeRunner.WIDTH / 2F) / (CubeRunner.WIDTH / 100F);
		if(f < -60F) f = -60F;
		if(f > 60F) f = 60F;
		rotation = f;
	}
	
	public boolean checkForCollision(Cube cube) {
		if(cube.posZ <= -4.5F && cube.posZ >= -5.75F && Math.abs(cube.posX - posX) <= 1F) return true;
		if(cube.posZ <= -5.75F && cube.posZ >= -7F && Math.abs(cube.posX - posX) <= 1.5F) return true;
		return false;
	}
	
	public float posX;
	private float rotation;
}
