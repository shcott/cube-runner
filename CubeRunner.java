import java.util.ArrayList;
import java.util.Random;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.util.glu.GLU;
import static org.lwjgl.opengl.GL11.*;

public class CubeRunner {
	
	public CubeRunner() {
		running = true;
		WIDTH = 800;
		HEIGHT = 600;
		slowMotionZoom = 0F;
		plainRed = 0.25F;
		plainGreen = 0.25F;
		plainBlue = 0.25F;
		slowMotion = false;
		rand = new Random();
		player = new Player();
		cubeList = new ArrayList<Cube>();
	}
	
	private void init() {
		try {
			/*Display.setFullscreen(true);
			WIDTH = Display.getDisplayMode().getWidth();
			HEIGHT = Display.getDisplayMode().getHeight();
			//*/Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			Display.setVSyncEnabled(true);
			Display.setTitle("Cube Runner");
			Display.create();
		} catch (LWJGLException e) {
			Sys.alert("Error", "Initialization failed!\n\n" + e.getMessage());
			System.exit(1);
		}
		
		glViewport(0, 0, WIDTH, HEIGHT); // Reset The Current Viewport
		glMatrixMode(GL_PROJECTION); // Select The Projection Matrix
		glLoadIdentity();
		GLU.gluPerspective(45F, ((float)WIDTH / (float)HEIGHT), 0.1F, 100F); // Calculate The Aspect Ratio Of The Window
		glMatrixMode(GL_MODELVIEW); // Select The Modelview Matrix
		glLoadIdentity();

		glShadeModel(GL_SMOOTH); // Enables Smooth Shading
		glClearColor(0.3F, 0.6F, 0.65F, 0F);
		glClearDepth(1F); // Depth Buffer Setup
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL); // The Type Of Depth Test To Do
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Really Nice Perspective Calculations
	}
	
	private void run() {
		init();
		Mouse.setCursorPosition(WIDTH / 2, HEIGHT / 2);
		Mouse.setGrabbed(true);
		while(running) {
			checkInput();
			if(Mouse.isGrabbed()) update();
			render();
			Display.update();
			//Display.sync(750);
		}
		Display.destroy();
	}
	
	private void checkInput() {
		if(Display.isCloseRequested()) running = false;
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))	Mouse.setGrabbed(false);
		if(Mouse.isButtonDown(0) && !Mouse.isGrabbed()) {
			Mouse.setCursorPosition(WIDTH / 2, HEIGHT / 2);
			Mouse.setGrabbed(true);
		}
	}
	
	private void update() {
		player.update(this);
		if(rand.nextInt(gameSpawnChance()) == 0) {
			Cube cube = new Cube(rand.nextFloat() * plainWidth() - rand.nextFloat() * plainWidth() + player.posX, plainLength());
			cubeList.add(cube);
		}
		slowMotion = Mouse.isButtonDown(1);
		if(slowMotion) {
			if(slowMotionZoom < 50) slowMotionZoom += 0.75F;
		} else {
			if(slowMotionZoom > 0) slowMotionZoom -= 0.75F;
			if(slowMotionZoom < 0) slowMotionZoom = 0;
		}
		if(rand.nextBoolean()) plainRed += (rand.nextFloat() - rand.nextFloat()) * 0.001F;
		if(rand.nextBoolean()) plainGreen += (rand.nextFloat() - rand.nextFloat()) * 0.001F;
		if(rand.nextBoolean()) plainBlue += (rand.nextFloat() - rand.nextFloat()) * 0.001F;
		if(plainRed > 0.3F) plainRed = 0.3F;
		if(plainRed < 0.2F) plainRed = 0.2F;
		if(plainGreen > 0.3F) plainGreen = 0.3F;
		if(plainGreen < 0.2F) plainGreen = 0.2F;
		if(plainBlue > 0.3F) plainBlue = 0.3F;
		if(plainBlue < 0.2F) plainBlue = 0.2F;
	}
	
	private void render() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
		glLoadIdentity();
		glPushMatrix(); {
			glTranslatef(0F, -0.5F + slowMotionZoom * 0.01F, -15F + slowMotionZoom * 0.04F);
			glRotatef(15F, 1F, 0F, 0F);
			//player
			glPushMatrix(); {
				glTranslatef(0F, -0.5F, 6F);
				glRotatef(-player.getRotation() / 7F, 0F, 0F, 1F);
				glBegin(GL_TRIANGLES);
				glColor3f(0.8F, 0.8F, 0.8F);
				glVertex3f(0F, 0F, -0.75F);
				glVertex3f(0.5F, 0F, 0.75F);
				glVertex3f(0F, 0.1F, 0F);
				glColor3f(0.7F, 0.7F, 0.7F);
				glVertex3f(0F, 0F, -0.75F);
				glVertex3f(-0.5F, 0F, 0.75F);
				glVertex3f(0F, 0.1F, 0F);
				glColor3f(1F, 1F, 1F);
				glVertex3f(0F, 0.1F, 0F);
				glVertex3f(0.5F, 0F, 0.75F);
				glVertex3f(0F, 0F, 0.5F);
				glColor3f(0.95F, 0.95F, 0.95F);
				glVertex3f(0F, 0.1F, 0F);
				glVertex3f(-0.5F, 0F, 0.75F);
				glVertex3f(0F, 0F, 0.5F);
				glEnd();
			} glPopMatrix();
			glRotatef(player.getRotation(), 0F, 0F, 1F);
			//plain
			glPushMatrix(); {
				glBegin(GL_QUADS);
				glColor3f(plainRed, plainGreen, plainBlue);
				glVertex3f(plainWidth() / 2F, -1F, plainLength());
				glVertex3f(-plainWidth() / 2F, -1F, plainLength());
				glVertex3f(-plainWidth() / 2F, -1F, -plainLength());
				glVertex3f(plainWidth() / 2F, -1F, -plainLength());
				glEnd();
			} glPopMatrix();
			//cubes
			for(int i = 0; i < cubeList.size(); i++) {
				glPushMatrix(); {
					Cube cube = cubeList.get(i);
					if(!updateCube(cube)) cubeList.remove(i);
					if(player.checkForCollision(cube)) cubeList.clear();
					glTranslatef(cube.posX - player.posX, 0F, -cube.posZ);
					drawCubeRGB(cube.getWidth() / 2F, cube.color[0], cube.color[1], cube.color[2]);
				} glPopMatrix();
			}
		} glPopMatrix();
	}
	
	private void drawCubeRGB(float length, float r, float g, float b) {
		glBegin(GL_QUADS);
		//front
		glColor3f(r, g, b);
		glVertex3f(length, length, length);
		glVertex3f(-length, length, length);
		glVertex3f(-length, -length, length);
		glVertex3f(length, -length, length);
		//back
		glColor3f(r - 0.4F, g - 0.4F, b - 0.4F);
		glVertex3f(-length, length, -length);
		glVertex3f(length, length, -length);
		glVertex3f(length, -length, -length);
		glVertex3f(-length, -length, -length);
		//top
		glColor3f(r - 0.1F, g - 0.1F, b - 0.1F);
		glVertex3f(length, length, -length);
		glVertex3f(-length, length, -length);
		glVertex3f(-length, length, length);
		glVertex3f(length, length, length);
		//bottom
		glColor3f(r - 0.3F, g - 0.3F, b - 0.3F);
		glVertex3f(length, -length, length);
		glVertex3f(-length, -length, length);
		glVertex3f(-length, -length, -length);
		glVertex3f(length, -length, -length);
		//right
		glColor3f(r - 0.2F, g - 0.2F, b - 0.2F);
		glVertex3f(length, length, -length);
		glVertex3f(length, length, length);
		glVertex3f(length, -length, length);
		glVertex3f(length, -length, -length);
		//left
		glColor3f(r - 0.25F, g - 0.25F, b - 0.25F);
		glVertex3f(-length, length, length);
		glVertex3f(-length, length, -length);
		glVertex3f(-length, -length, -length);
		glVertex3f(-length, -length, length);
		glEnd();
	}
	
	private boolean updateCube(Cube cube) {
		if(!Mouse.isGrabbed()) return true;
		cube.posZ -= gameSpeed();
		if(cube.posZ < -10) return false;
		return true;
	}
	
	private float gameSpeed() {
		if(slowMotion && slowMotionZoom >= 50) return 0.01F;
		return 0.075F;
	}
	
	private int gameSpawnChance() {
		int i = 25;
		if(slowMotion) i += 5;
		if(i < 1) i = 1;
		return 25;
	}
	
	private int plainLength() {
		return 60;
	}
	
	private int plainWidth() {
		return 80;
	}
	
	public static void main(String...args) {
		CubeRunner game = new CubeRunner();
		game.run();
	}
	
	public boolean slowMotion;
	public static Random rand;
	public static int WIDTH, HEIGHT;
	private float slowMotionZoom;
	private float plainRed, plainGreen, plainBlue;
	private boolean running;
	private Player player;
	private ArrayList<Cube> cubeList;
}