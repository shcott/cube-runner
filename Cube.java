public class Cube {
	
	public Cube() {
		posX = 0;
		posZ = 0;
		width = 2F;
		color[0] = 0.25F + CubeRunner.rand.nextFloat() * 0.65F;
		color[1] = 0.25F + CubeRunner.rand.nextFloat() * 0.65F;
		color[2] = 0.25F + CubeRunner.rand.nextFloat() * 0.65F;
		dead = false;
	}
	
	public Cube(float x, float z) {
		this();
		posX = x;
		posZ = z;
	}
	
	public Cube(float r, float g, float b, float x, float z) {
		this(x, z);
		color[0] = r;
		color[1] = g;
		color[2] = b;
	}
	
	public float getWidth() {
		return width;
	}
	
	public float posX, posZ;
	public float[] color = new float[3];
	public float dieScale;
	public boolean dead;
	private float width;
}